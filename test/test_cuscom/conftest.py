import pytest
from airflow.models import DAG
from airflow.utils.dates import days_ago
from cuscom import CustomKubernetesPodOperator


@pytest.fixture
def dag():
    dag_arguments = {
        "dag_id": "test_dag_id",
        "schedule_interval": None,
        "catchup": False,
        "max_active_runs": 4,
        "start_date": days_ago(2),
    }
    return DAG(**dag_arguments)


@pytest.fixture
def operator_defaults():
    return {"task_id": "test_task_id", "name": "test_name", "on_failure_callback": None}


@pytest.fixture
def create_operator(dag, operator_defaults):
    def fn(**kwargs):
        return CustomKubernetesPodOperator(dag=dag, **{**operator_defaults, **kwargs})

    return fn
