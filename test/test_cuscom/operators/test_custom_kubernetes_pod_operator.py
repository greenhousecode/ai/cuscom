class TestCustomKubernetesPodOperator:
    def test_add_conf_items_to_arguments(self, create_operator):
        operator = create_operator()
        operator.conf = {"test_key": "test_value"}
        assert operator.arguments == ["--test_key", "test_value"]

    def test_use_output_of_argument_fn_as_arguments(self, create_operator):
        operator = create_operator(arguments_fn=lambda s: ["--argument_fn", s["test_key_1"]])
        operator.conf = {"test_key_1": ["test_value_1", "test_value_2"], "test_key_2": ["blah"]}
        assert operator.arguments == ["--argument_fn", ["test_value_1", "test_value_2"]]

    def test_extend_arguments_with_output_of_argument_fn(self, create_operator):
        operator = create_operator(arguments_fn=lambda s: ["--argument_fn", s["test_key"]])
        operator.arguments = ["src"]
        operator.conf = {"test_key": ["test_value_1", "test_value_2"]}
        assert operator.arguments == ["src", "--argument_fn", ["test_value_1", "test_value_2"]]

    def test_arguments_fn_empty_conf(self, create_operator):
        operator = create_operator(arguments_fn=lambda s: ["--argument_fn", s["test_key"]])
        assert operator.arguments == []
