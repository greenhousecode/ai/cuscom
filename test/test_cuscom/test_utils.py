import pytest

from cuscom.utils import SENSITIVE_MASK
from cuscom.utils import flatten_dict, default_arguments_fn, from_kebab_case_key, mask


class TestUtils:
    @pytest.mark.parametrize(
        "nested_dict, separator, expected",
        [
            ({"task": {"id": 1}}, ".", {"task.id": 1}),
            ({"task": {"id": 1}}, "_", {"task_id": 1}),
            (
                {"task": {"id": 1, "name": "task_name"}},
                ".",
                {"task.id": 1, "task.name": "task_name"},
            ),
            (
                {"task": {"id": 1, "name": {"main": "main_name", "sub": "sub_name"}}},
                ".",
                {"task.id": 1, "task.name.main": "main_name", "task.name.sub": "sub_name"},
            ),
        ],
    )
    def test_flatten_dict(self, nested_dict, separator, expected):
        actual = flatten_dict(nested_dict, separator)
        assert actual == expected

    @pytest.mark.parametrize(
        "conf, expected",
        [
            (
                {"key_1": "value_1", "key_2": "value_2"},
                ["--key_1", "value_1", "--key_2", "value_2"],
            ),
            ({"key_1": 10, "key_2": "value_2"}, ["--key_1", "10", "--key_2", "value_2"]),
            (
                {"key_1": [10, 11], "key_2": "value_2"},
                ["--key_1", "[10, 11]", "--key_2", "value_2"],
            ),
        ],
    )
    def test_default_arguments_fn(self, conf, expected):
        actual = default_arguments_fn(conf)
        assert actual == expected

    @pytest.mark.parametrize(
        "input, expected",
        [({"key-1": "value_1", "key-2": 10}, {"key_1": "value_1", "key_2": 10})],
    )
    def test_from_kebab_case(self, input, expected):
        actual = from_kebab_case_key(input)
        assert actual == expected

    @pytest.mark.parametrize(
        "input, expected",
        [
            ({"key_1": "value_1", "key_2": 10}, {"key_1": "value_1", "key_2": 10}),
            ({"secrets": "value_1", "key_2": 10}, {"secrets": SENSITIVE_MASK, "key_2": 10}),
            (
                {"secret_credentials": "value_1", "key_2": 10},
                {"secret_credentials": SENSITIVE_MASK, "key_2": 10},
            ),
            ({"SECRETS": "value_1", "key_2": 10}, {"SECRETS": SENSITIVE_MASK, "key_2": 10}),
            (None, None),
        ],
    )
    def test_mask(self, input, expected):
        actual = mask(input)
        assert actual == expected
