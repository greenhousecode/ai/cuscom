# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2023-06-15

## Fixed

- Fix trigger_dag
- Add exception logs when triggering fails

## [1.0.0] - 2023-06-05

## Changed

- Use cuscom with airflow >= 2.x.x
- Updated references to airflow providers
- Kubernetes components from kubernetes library

## Deprecated

- As of 1.x.x, cuscom will only work with airflow >= 2.x.x


## [0.2.6] - 2021-12-29


## [0.2.4] - 2021-12-28

## Fixed

- Skip mask if config is None

## [0.2.3] - 2021-12-28

## Fixed

- Create config copy for mask

## [0.2.2] - 2021-12-28

## Fixed

- Fix getting conf from dagrun

## [0.2.1] - 2021-12-28

## Added

- Mask sensitive data in configs.
- Export `hooks` from root of package.
- Add only non-empty argument to slack message.

## [0.2.0] - 2021-12-07

## Added

- Export `trigger_dag` from root of package.

## [0.1.0] - 2021-12-07

## [0.0.1] - 2021-11-25

### Added

- Added operator: `CustomKubernetesOperator`
- Added utils `flatten_dict`
